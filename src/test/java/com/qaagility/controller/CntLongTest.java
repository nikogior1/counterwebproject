package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class CntLongTest {

        @Test
        public void testDiv() throws Exception {
		int k= new CntLong().divideNums(4,2);
                assertEquals("DivisionResultTest", 2, k);
        }
	
	@Test
        public void testDivZero() throws Exception {
                int k= new CntLong().divideNums(4,0);
                assertEquals("DivisionZeroResultTest", Integer.MAX_VALUE, k);
        }


}

